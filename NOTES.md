# Markup (Input)

A simple language for marking up sequences of questions as well as how to handle them would be useful. The S-Expression syntax may be the easiest in the long run, as it provides structure as well as flow.

# Output

Currently, I just interactively run things; this kind of test environment will be useful, but generating HTML/CSS/JavaScript as output is the likely path we'll follow. [PhoneGap](http://phonegap.com/) could be a nice way to get to mobile devices and apps that "run" the survey.

# Storage

[Parse](https://parse.com/) provides a simple object store that can be used to back web-based apps very nicely. Or, mobile apps for that matter. It would be a quick-and-dirty way to get storage. Another would be to use Google Spreadsheets as a simple database.
